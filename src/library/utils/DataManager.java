package library.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


/**
 *
 */
public class DataManager {


    /**
     * Serializes entity to file with specified file name
     *
     * @param entity   entity to serialize
     * @param fileName file name
     * @param <T>
     */
    public static <T extends Serializable> void serializeEntityToFile(T entity, String fileName) {
        try {
            serializeEntity(entity, new FileOutputStream(fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Serializes entity to any kind of OutputStream
     *
     * @param entity entity to serialize
     * @param os     {@link OutputStream} to serialize entity
     * @param <T>
     */
    public static <T extends Serializable> void serializeEntity(T entity, OutputStream os) {
        try (ObjectOutputStream oos = new ObjectOutputStream(os)) {

            oos.writeObject(entity);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deserialize entity from file with specified filename
     *
     * @param entityType kind of entity class
     * @param filename   file name
     * @param <T>
     * @return new deserialized entity instance or null if serialization failed
     */
    public static <T extends Serializable> T deserializeEntityFromFile(Class<T> entityType, String filename) {
        T entity = null;
        try (FileInputStream fis = new FileInputStream(filename)) {
            entity = deserializeEntity(entityType, fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return entity;
    }

    /**
     * Deserialize entity from any kind of InputStream
     *
     * @param entityType kind of entity class
     * @param is         {@link InputStream}
     * @param <T>
     * @return new deserialized entity instance or null if serialization failed
     */
    @SuppressWarnings("unchecked")
    public static <T extends Serializable> T deserializeEntity(Class<T> entityType, InputStream is) {
        T entity = null;
        try (ObjectInputStream ois = new ObjectInputStream(is)) {
            Object object = ois.readObject();
            if (object.getClass().equals(entityType)) {
                entity = (T) object;
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return entity;
    }

    /**
     * Serializes collection with entities to specified file, or to file with
     * title <code>entityType.getSimpleName().toLowerCase() + "s.txt"</code> if fileName is null
     *
     * @param entityType kind of entity class
     * @param entities   collection with entities
     * @param fileName   file name, may be null
     * @param <T>
     */
    public static <T extends Serializable> void serializeEntitiesToFile(Class<T> entityType, Collection<T> entities,
                                                                        String fileName) {

        if (fileName == null) {
            fileName = entityType.getSimpleName().toLowerCase() + "s.txt";
        }

        try (FileOutputStream fos = new FileOutputStream(fileName)) {
            serializeEntities(entities, fos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Serializes collection with entities to specified OutputStream
     *
     * @param entities collection with entities
     * @param os       {@link OutputStream} to serialize entity
     * @param <T>
     */
    public static <T extends Serializable> void serializeEntities(Collection<T> entities,
                                                                  OutputStream os) {
        try (ObjectOutputStream oos = new ObjectOutputStream(os)) {

            oos.writeInt(entities.size());
            for (T element : entities) {
                oos.writeObject(element);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deserializes set with entities from specified file, or from file
     * <code>entityType.getSimpleName().toLowerCase() + "s.txt"</code> if fileName is null
     *
     * @param entityType kind of entity class
     * @param fileName   file name
     * @param <T>
     * @return set with deserialized entities, set may be empty
     */
    public static <T extends Serializable> Set<T> deserializeEntitiesFromFile(Class<T> entityType,
                                                                              String fileName) {
        Set<T> entities = new HashSet<>();
        if (fileName == null) {
            fileName = entityType.getSimpleName().toLowerCase() + "s.txt";
        }
        try (FileInputStream fis = new FileInputStream(fileName);) {
            entities.addAll(deserializeEntities(entityType, fis));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return entities;
    }


    /**
     * Deserializes set with entities from specified InputStream
     *
     * @param entityType kind of entity class
     * @param is         {@link InputStream}
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T extends Serializable> Set<T> deserializeEntities(Class<T> entityType,
                                                                      InputStream is) {

        Set<T> entities = new HashSet<>();
        try (ObjectInputStream ois = new ObjectInputStream(is)) {

            int elementsCount = ois.readInt();
            for (int i = 0; i < elementsCount; i++) {
                Object object = ois.readObject();
                if (object.getClass().equals(entityType)) {
                    T entity = (T) object;
                    entities.add(entity);
                }
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return entities;
    }
}
