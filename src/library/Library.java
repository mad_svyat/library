package library;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import library.models.Book;
import library.models.BookInstance;
import library.models.Booking;
import library.models.Reader;

/**
 *
 */
public class Library {

    private Set<Book> catalog;
    private Set<BookInstance> store;
    private Set<Reader> readers;
    private Set<Booking> bookings;

    public Library() {
        catalog = new HashSet<>(1024);
        store = new HashSet<>(4096);
        readers = new HashSet<>(512);
        bookings = new HashSet<>(2048);
    }

    public void buyBook(String title, String author, String isbn,
                        int count, int year) {
        Book book = new Book(author, title, year, isbn);
        catalog.add(book);

        for (int i = 0; i < count; i++) {
            BookInstance bookInstance = new BookInstance(book, UUID.randomUUID());
            store.add(bookInstance);
        }
    }

    public void takeBook(String firstName, String secondName,
                         String lastName, long passportNumber, String title) {
        Object[] reader = readers.stream()
                .filter(r -> r.getPassportNumber() == passportNumber)
                .toArray();

        Reader tempReader;
        if (reader.length != 0) {
            tempReader = (Reader) reader[0];
        } else {
            tempReader = new Reader(firstName, secondName, lastName, passportNumber);
            readers.add(tempReader);
        }
        BookInstance bookInstance = (BookInstance) store.stream()
                .filter(b -> b.getBook().getTitle().equals(title)).toArray()[0];

        if (bookInstance == null) {
            System.out.println("no such book");
            return;
        }
        Booking booking = new Booking(bookInstance, tempReader, new Date(System.currentTimeMillis()),
                new Date(System.currentTimeMillis()));

        bookings.add(booking);
        store.remove(bookInstance);
    }

    public void returnBook(String firstName, String secondName,
                           String lastName, long passportNumber, String title) {

        Reader reader = new Reader(firstName, secondName, lastName, passportNumber);
        Booking booking = (Booking) bookings.stream()
                .filter(b -> b.getBookInstance().getBook().getTitle().equals(title)
                        && b.getReader().equals(reader)).toArray()[0];

        if (booking == null) {
            System.out.println("no such booking");
            return;
        }

        store.add(booking.getBookInstance());
        bookings.remove(booking);
    }

    public void showAllData() {
        System.out.println("CATALOG:");
        catalog.forEach(System.out::println);
        System.out.println("\nBOOKINGS:");
        bookings.forEach(System.out::println);
        System.out.println("\nSTORE:");
        store.forEach(System.out::println);
        System.out.println("\nREADERS:");
        readers.forEach(System.out::println);
    }

    public Set<Book> getCatalog() {
        return catalog;
    }

    public void setCatalog(Set<Book> catalog) {
        this.catalog = catalog;
    }
}
