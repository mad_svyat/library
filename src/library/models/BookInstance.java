package library.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 */
public class BookInstance implements Serializable {

    private static final long serialVersionUID = 2L;
    private Book book;
    private UUID number;
    private List<Booking> orderHistory;

    public BookInstance(Book book, UUID number) {
        this.book = book;
        this.number = number;

        orderHistory = new ArrayList<>(32);
    }

    @Override
    public int hashCode() {
        return number.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof BookInstance)) {
            return false;
        }

        if (!number.equals(((BookInstance) obj).number)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return book.toString() + '@' + number;
    }

    public Book getBook() {
        return book;
    }
}
