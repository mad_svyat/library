package library.models;


import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;


/**
 *
 */
public class Reader implements Externalizable {

    private static final String SIGNATURE = "com.madsvyat";
    private static final long serialVersionUID = 2L;

    private String firstName;
    private String secondName;
    private String lastName;
    private long passportNumber;

    public Reader() {}

    public Reader(String firstName, String secondName,
                  String lastName, long passportNumber) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.passportNumber = passportNumber;
    }

    @Override
    public int hashCode() {
        return (int) (31 * passportNumber);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return  false;
        }

        if (!(obj instanceof Reader)) {
            return false;
        }

        if (passportNumber != ((Reader) obj).passportNumber) {
            return false;
        }

        return true;
    }

    public long getPassportNumber() {
        return passportNumber;
    }

    @Override
    public String toString() {
        return "Reader{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", passportNumber=" + passportNumber +
                '}';
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(firstName);
        out.writeUTF(secondName);
        out.writeUTF(lastName);
        out.writeLong(passportNumber);
        out.writeUTF(SIGNATURE);

    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        firstName = in.readUTF();
        secondName = in.readUTF();
        lastName = in.readUTF();
        passportNumber = in.readLong();
        String signature = in.readUTF();
        System.out.println(signature);
    }
}
