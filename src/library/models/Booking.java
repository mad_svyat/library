package library.models;

import java.io.Serializable;
import java.util.Date;

/**
 *
 */
public class Booking implements Serializable {

    private static final long serialVersionUID = 2L;

    private BookInstance bookInstance;
    private Reader reader;
    private Date startDate;
    private Date finishDate;
    private Date returnDate;

    public Booking(BookInstance bookInstance, Reader reader,
                   Date startDate, Date finishDate) {
        this.bookInstance = bookInstance;
        this.reader = reader;
        this.startDate = startDate;
        this.finishDate = finishDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    @Override
    public int hashCode() {
        return 31 * bookInstance.hashCode() + reader.hashCode() +
                startDate.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof Booking)) {
            return false;
        }

        Booking another = (Booking) obj;
        if (!(bookInstance.equals(another.bookInstance) &&
                reader.equals(another.reader) &&
                startDate.equals(another.startDate))) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "bookInstance=" + bookInstance +
                ", reader=" + reader +
                ", startDate=" + startDate +
                ", finishDate=" + finishDate +
                ", returnDate=" + returnDate +
                '}';
    }

    public BookInstance getBookInstance() {
        return bookInstance;
    }

    public Reader getReader() {
        return reader;
    }
}
