package library.models;

import java.io.Serializable;

/**
 *
 */
public class Book implements Serializable {

    private static long serialVersionUID = 2L;
    private String author;
    private String title;
    private int year;
    private String isbn;

    public Book(String author, String title, int year, String isbn) {
        this.author = author;
        this.title = title;
        this.year = year;
        this.isbn = isbn;
    }

    @Override
    public int hashCode() {
        return isbn.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof Book)) {
            return false;
        }

        if (!isbn.equals(((Book)obj).isbn)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return author + '@' + title + '@' + year + '@' + isbn;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getYear() {
        return year;
    }

    public String getIsbn() {
        return isbn;
    }
}
