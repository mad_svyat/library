package com.madsvyat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import library.Library;
import library.models.Book;
import library.models.Reader;
import library.utils.DataManager;

public class Main {

    public static void main(String[] args) {

        testReaderSerialization();
        testReadersSerialization();
        testBooksSerialization();
        System.out.println();


        Library library = new Library();

        for (Book book : DataManager.deserializeEntitiesFromFile(Book.class, null)) {
            library.buyBook(book.getTitle(), book.getAuthor(), book.getIsbn(), 1, book.getYear());
        }

        Reader john = new Reader("John", "Connor", "Kailovich", 179984378L);
        Reader sarah = new Reader("Sarah", "Connor", "Unknown", 389024309L);

        library.buyBook("Intro to Java", "Shildt", "12231jkfk", 5, 2017);
        library.buyBook("Spring in action", "Walls", "99398oplk", 5, 2015);
        library.takeBook("John", "Connor", "Kailovich", 179984378L, "Intro to Java");
        library.takeBook("Sarah", "Connor", "Unknown",
                3890L, "Spring in action");
        library.returnBook("Sarah", "Connor", "Unknown",
                3890L, "Spring in action");
        library.showAllData();

        DataManager.serializeEntitiesToFile(Book.class, library.getCatalog(), null);


    }

    private static void testReaderSerialization() {
        Reader john = new Reader("John", "Connor", "Kailovich", 179984378L);
        System.out.println("Before serialization: " + john);
        DataManager.serializeEntityToFile(john, "john.txt");

        Reader deserializedJohn = DataManager.deserializeEntityFromFile(Reader.class,"john.txt");
        System.out.println("after serialize/deserialize: " + deserializedJohn);
    }

    private static void testReadersSerialization() {
        Set<Reader> readers = new HashSet<>();
        Reader john = new Reader("John", "Connor", "Kailovich", 179984378L);
        Reader sarah = new Reader("Sarah", "Connor", "Unknown", 389024309L);
        readers.add(sarah);
        readers.add(john);

        System.out.println("Before serialization:");
        printEntities(readers);

        DataManager.serializeEntitiesToFile(Reader.class, readers, null);

        Set<Reader> restoredReaders = DataManager.deserializeEntitiesFromFile(Reader.class, null);
        System.out.println("after serialize/deserialize:");
        printEntities(restoredReaders);
    }

    private static void testBooksSerialization() {
        Book shildt = new Book("Shildt", "Intro to Java", 2017,"12231jkfk");
        Book springBook = new Book("Walls", "Spring in action",2015, "99398oplk");
        List<Book> books = new ArrayList<>();
        books.add(shildt);
        books.add(springBook);

        System.out.println("Before serialization:");
        printEntities(books);

        DataManager.serializeEntitiesToFile(Book.class, books, null);

        Set<Book> restoredBooks = DataManager.deserializeEntitiesFromFile(Book.class, null);
        System.out.println("after serialize/deserialize:");
        printEntities(restoredBooks);
    }

    private static  <T extends Serializable> void printEntities(Collection<T> elements) {
        System.out.println("collection size: " + elements.size());
        elements.forEach(System.out::println);
    }
}
